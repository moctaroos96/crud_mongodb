require('./models/db');

const express = require("express");
const path = require("path");
const exphbs = require('express-handlebars');
const bodyparser = require('body-parser');

const volController = require("./controllers/volController");

var app = express();
app.use(bodyparser.urlencoded({
    extended: true
}));

app.set('views', path.join(__dirname, '/views/'));
app.engine('hbs', exphbs({ extname: 'hbs', defaultLayout: 'mainLayout', layoutsDir: __dirname + '/views/layouts/', 
}));

app.set('view engine', 'hbs');

app.listen(2000, () => {
    console.log("Serveur Express demarre sur le port : 2000");
});

app.use("/vol", volController);