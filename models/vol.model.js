const mongoose = require("mongoose");

var volSchema = new mongoose.Schema({
    id_agence : {
        type : String
    },
    date_depart : {
        type : String,
        required : 'Champ obligatoire.'
    },
    ville_depart : {
        type : String ,
        required : 'Champ obligatoire.'
    },
    ville_arrivee : {
        type : String,
        required : 'Champ ibligatoire'
    },
    type : {
        type : String
    }
        
}, {
    toObject: {
        virtuals: true,
      },
      toJSON: {
        virtuals: true,
      }
});


mongoose.model('Vol', volSchema);