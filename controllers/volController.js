const express = require("express");

var router = express.Router();
const mongoose = require("mongoose");

const Vol = mongoose.model('Vol');



router.get('/', (req, res)=>{
    res.render("vol/addOrEdit", {
        viewTitle : "Insertion Vol"
    });
});

router.post('/', (req, res)=>{
    if(req.body._id == '')
    insertRecord(req, res);
    else
    updateRecord(req, res);
});

function insertRecord(req, res){
    var vol = new Vol();
    vol.agence = req.body.agence;
    vol.date_depart = req.body.date_depart;
    vol.ville_depart = req.body.ville_depart;
    vol.ville_arrivee = req.body.ville_arrivee;
    vol.type = req.body.type; 
    vol.save((err, doc) => {
        if(!err)
            res.redirect('vol/list');
        else{
            if(err.name == 'ValidationError'){
                handleValidationError(err, req.body);
                res.render("vol/addOrEdit", {
                    viewTitle : "Isertion Vol",
                    vol : req.body
                });
            }   
            else{
                console.log('Erreur durant insertion : ' + err);
            }
        }
    });
}

function updateRecord(req, res) {
    Vol.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, doc) => {
        if (!err) { res.redirect('vol/list'); }
        else {
            if (err.name == 'ValidationError') {
                handleValidationError(err, req.body);
                res.render("vol/addOrEdit", {
                    viewTitle: 'Update Vol',
                    vol: req.body
                });
            }
            else
                console.log('Error during record update : ' + err);
        }
    });
}

router.get('/list', (req, res) => {
    Vol.find((err, docs)=>{
        if(!err) {
            res.render("vol/list", {
                list : docs
            });
        }
        else{
            console.log("Erreur durant la selection des vols : " + vol);
        }
    }).lean()
})

function handleValidationError(err, body){
    for(field in err.errors) {
        switch (err.errors[field].path) {
            case 'date_depart':
                body['date_departError'] = err.errors[field].message;
                break;
            case 'ville_depart':
                body['ville_departError'] = err.errors[field].message;
                break;
            case 'ville_arrivee':
                body['ville_arriveeError'] = err.errors[field].message;
                break;
            default:
                break;
        }
    }
}

router.get('/:id', (req, res) => {
    Vol.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.render("vol/addOrEdit", {
                viewTitle: "Update Vol",
                vol: doc
            });
        }
    }).lean()
});

router.get('/delete/:id', (req, res) => {
    Vol.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) {
            res.redirect('/vol/list');
        }
        else { console.log('Error in vol delete :' + err); }
    });
});

module.exports = router;